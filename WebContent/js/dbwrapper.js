/*
 * DbWrapperクラス(Javascript)
 * driver.php, dbwrapper.phpに依存
 * ※PHPの実行環境(Apache等）が必要
 * ※Tomcatでは動かすのが困難
 */
class DbWrapper {

    constructor(table, id, callback) {
        this.table = table;
    	$.getJSON( 'driver.php', { table: this.table, method: 'get', id: id }, callback );
    }
    getAll(params, callback) {
    	if ( params == null ) params = {};
    	params.table = this.table;
    	params.method = 'getAll';
    	$.getJSON( 'driver.php', params, callback );
  	}
  	insertObject(params, callback) {
	    params.table = this.table;
	    params.method = 'insert';
	    $.getJSON( 'driver.php', params, callback );
  	}
  	updateObject(id, params, callback) {
  		params.table = this.table;
  		params['id'] = id;
  		params.method = 'insert';
  		$.getJSON( 'driver.php', params, callback );
  	}
  	deleteObject(id, callback) {
  		    $.getJSON( 'driver.php', { table:this.table,
  			        'id':id, method: 'delete' }, callback );
  	}
}







