
jQuery(function ($) {
  'use strict';
  var ENTER_KEY = 13;
  var ESCAPE_KEY = 27;

  var util = {
    /*
     * ランダムなUUIDを求める
     */
//    uuid: function () {
//
//      var i, random;
//      var uuid = '';
//
//      for (i = 0; i < 32; i++) {
//        //random = Math.random() * 16 | 0;
//        var random = Math.floor( Math.random() * (16 - 0 + 1)) + 0; // 16から0までの整数値をランダムに取得
//        if (i === 8 || i === 12 || i === 16 || i === 20) {
//          uuid += '-';
//        }
//        /*
//         * &  ビット論理積 (AND) です。
//         * |  ビット論理和 (OR) です。
//         * (condition : ifTrue ? IfFalse)  三項演算子です。
//         * toString(16)  16進数表示
//         *
//         */
//        uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random)).toString(16);
//      }
//      return uuid;
//    },
    /*
     * 単数形か複数形かを直す
     */
//    pluralize: function (count, word) {
//      return count === 1 ? word : word + 's';
//    },
    /*
     * データベースへの接続
     */
	store: function (namespace, data) {
		if (arguments.length > 1) {
			return localStorage.setItem(namespace, JSON.stringify(data));
		} else {
			var store = localStorage.getItem(namespace);
			return (store && JSON.parse(store)) || [];
		}
	},
    /*
     * 8桁のランダムな数字の組み合わせを作る
     */
    random8: function () {
    	return Math.floor(Math.random() * (99999999 - 0 + 1) + 0);
    }
  };

  var App = {
	/*
	 * 初期化する
  	 */
	init: function () {
		//this.todos = util.store('todos-jquery');
		//his.todoTemplate = Handlebars.compile($('#todo-template').html());
		//this.footerTemplate = Handlebars.compile($('#footer-template').html());
		this.bindEvents();

//		new director.http.Router({
//			'/:filter': function (filter) {
//				this.filter = filter;
//				this.render();
//			}.bind(this)
//		}).init('/all');   // 全選択でスタートする
	},
	/*
	 * マウスイベントを登録する
	 */
	bindEvents: function () {
		$('#new-todo').on('keyup', this.create.bind(this));
		$('#toggle-all').on('change', this.toggleAll.bind(this));
		$('#footer').on('click', '#clear-completed', this.destroyCompleted.bind(this));

		$('#todo-list')
			.on('change', '.toggle', this.toggle.bind(this))
			.on('dblclick', 'label', this.editingMode.bind(this))
			.on('keyup', '.edit', this.editKeyup.bind(this))
			.on('focusout', '.edit', this.update.bind(this))
			.on('click', '.destroy', this.destroy.bind(this));
	},
	/*
	 * #main のレンダリング
	 */
	render: function () {
		var todos = this.getFilteredTodos();
		$('#todo-list').html(this.todoTemplate(todos));
		$('#main').toggle(todos.length > 0);
		$('#toggle-all').prop('checked', this.getActiveTodos().length === 0);
		this.getFilterValue();
		this.renderFooter();
		$('#new-todo').focus();
		util.store('todos-jquery', this.todos);
	},
	/*
	 * #footerのレンダリング
	 */
	renderFooter: function () {
		var todoCount = this.todos.length;
		var activeTodoCount = this.getActiveTodos().length;
		var template = this.footerTemplate({
			activeTodoCount: activeTodoCount,
			//activeTodoWord: util.pluralize(activeTodoCount, 'item'),
			completedTodos: todoCount - activeTodoCount,
			filter: this.filter
		});
		$('#footer').toggle(todoCount > 0).html(template);
	},
	/*
	 * 現在のフィルターの値を取得する
	 */
	getFilterValue: function () {
		//filter = $('[class="selected"]').children('[name="menu" ]').$('[value]').get();
		filter = $('[id="header"]').$('[class]').get().
		this.filter = filter;
	},
	/*
	 * 全て完了にする
	 */
	toggleAll: function (e) {
		var isChecked = $(e.target).prop('checked');
		this.todos.forEach(function (todo) {
			todo.completed = isChecked;
		});
		this.render();
	},
	/*
	 * 未完了だけ選択する
	 */
	getActiveTodos: function () {
		return this.todos.filter(function (todo) {
			location.reload();
			return !todo.completed;
		});
	},
	/*
	 * 完了したものだけ選択する
	 */
	getCompletedTodos: function () {
		return this.todos.filter(function (todo) {
			location.reload();
			return todo.completed;
		});
	},
	/*
	 * リスト表示を切り替えるフィルター
	 */
	getFilteredTodos: function () {
		if (this.filter === 'active') {
			return this.getActiveTodos();
		}
		if (this.filter === 'completed') {
			return this.getCompletedTodos();
		}

		return this.todos;
	},
	/*
	 * 完了したものをフィルターして見えなくする
	 */
	destroyCompleted: function () {
		this.todos = this.getActiveTodos();
		this.filter = 'all';
		this.render();
	},
	/*
	 *
	 */
	// accepts an element from inside the `.item` div and
	// returns the corresponding index in the `todos` array
	getIndexFromEl: function (el) {
		var id = $(el).closest('li').data('id');
		var todos = this.todos;
		var i = todos.length;
		while (i--) {
			if (todos[i].id === id) {
				return i;
			}
		}
	},
	/*
	 * エンターキーが押されると新規作成する
	 */
	create: function (e) {
		var $input = $(e.target);
		var val = $input.val().trim();
		if (e.which !== ENTER_KEY || !val) {
			return;
		}
		this.todos.push({
			//id: util.uuid(),
			id: util.random8(),
			title: val,
			completed: false
		});
		$input.val('');
		this.render();
	},
	/*
	 * 完了と未完了を切り替える
	 */
	toggle: function (e) {
		var i = this.getIndexFromEl(e.target);
		this.todos[i].completed = !this.todos[i].completed;
		this.render();
	},
	/*
	 * 編集モードに入る
	 */
	editingMode: function (e) {
		var $input = $(e.target).closest('li').addClass('editing').find('.edit');
			$input.val($input.val()).focus();
	},
	/*
	 * 編集モードから出る
	 */
	editKeyup: function (e) {
		if (e.which === ENTER_KEY) {
			e.target.blur();
		}
		if (e.which === ESCAPE_KEY) {
			$(e.target).data('abort', true).blur();
		}
	},
	/*
	 * データを更新する
	 */
	update: function (e) {
		var el = e.target;
		var $el = $(el);
		var val = $el.val().trim();
		if (!val) {
			this.destroy(e);
			return;
		}
		if ($el.data('abort')) {
			$el.data('abort', false);
		} else {
			this.todos[this.getIndexFromEl(el)].title = val;
		}
		this.render();
	},
	/*
	 * 1つのレコードを削除する(表示しなくさせる)
	 */
	destroy: function (e) {
		this.todos.splice(this.getIndexFromEl(e.target), 1);
		this.render();
	}
  };

  /*
   * 初期化実行
   */
  App.init();
});
