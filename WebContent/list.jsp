<%
//PrintWriter writer = response.getWriter(); // JSPの暗黙オブジェクト: outで代用。
String activeTodoWord = "アイテム";
String filter = (String)request.getAttribute("filter");
%>
<section id="todoapp">
	<header id="header" class="<%=filter %>">
		<h1>ToDoの一覧</h1>
		<form action="create" method="post">
			<!-- <div id="toggle-all-view">
				<input id="toggle-all" type="checkbox" name="toggle-all" value="1">
				<label for="toggle-all">すべて完了にする</label>
			</div> -->
			<input id="new-todo" placeholder="やることを書いてみましょう" autofocus type="text" name="new-title" style="display:inline;">
			<input type="submit" value="新規登録" style="display:inline;">
		</form>
	</header>
	<section id="main">

		<ul id="todo-list">
<%
List<ToDoPage> list = ToDoPage.autocast(request.getAttribute("list"));
if(list != null) {
for(ToDoPage todoPage : list ) {
	String id = todoPage.getId();
    String title =todoPage.getTitle();
    boolean completed = todoPage.isCompleted();
    String content = null;
    if(todoPage.getContent() != null) {
    	content = todoPage.getContent();
    }
	%>
	<% if(completed) { %>
		<li class="completed" data-id="<%= id %>">
	<%} else {%>
		<li data-id="<%= id %>">
	<%} %>
		<div class="view">
			<form action="update" method="post">
				<input class="toggle" type="checkbox" <%if(completed) { out.println("checked"); } %> name="checked" style="display:inline;" value="1">
				<label style="display:inline;"><%= title %></label>
				<input type="hidden" name="id" value="<%= id %>" />
				<input type="hidden" name="title" value="<%= title %>" />
				<input type="hidden" name="content" value="<%= content %>" />
				<input type="hidden" name="completed" value="<%= completed %>" />
				<!-- <button class="destroy"></button> -->
				<input type="submit" value="更新" style="display:inline;">
			</form>
		</div>
   		<input class="edit" value="<%= title %>">
   		</li>
	<%}
	}%>
		</ul>
	</section>
	<footer id="footer">
	<%
	int allToDos = 0;
	int activeTodoCount = 0;
	int completedTodos = 0;
	if(list != null) {
		for(ToDoPage todoPage : list ) {
		    boolean completed = todoPage.isCompleted();
		    if(completed) {
		    	completedTodos++;
		    } else {
		    	activeTodoCount++;
		    }
		    allToDos++;
		}
	} %>
		<span id="todo-count"><strong><%= activeTodoCount %></strong> <%= activeTodoWord %> あります</span>
		<ul id="filters">
			<li>
				<form action="all" method="post" <% if(filter == "all") {%>class="selected"<%} %> style="display:inline;">
				<input type="hidden" name="menu" value="all" />
				<input type="submit" value="全て" />
				</form>
			</li>
			<li>
				<form action="active" method="post" <% if(filter == "active") {%>class="selected"<%} %> style="display:inline;">
				<input type="hidden" name="menu" value="active" />
				<input type="submit" value="未完了のみ"/>
				</form>
			</li>
			<li>
				<form action="completed" method="post" <% if(filter == "completed") {%>class="selected"<%} %> style="display:inline;">
				<input type="hidden" name="menu" value="completed" />
				<input type="submit" value="完了のみ"/>
				</form>
			</li>
		</ul>
		<% if(completedTodos > 0) {%>
			<form action="delete" method="post">
			<input type="hidden" name="menu" value="delete" />
			<button id="clear-completed" type="submit">完了済みを削除する</button>
			</form>
		<%} %>
	</footer>
</section>
<footer id="info">
	<!-- <p>ダブルクリックをするとTODOを編集できます</p> -->
	<p>「完了済みを削除する」ボタンをクリックすると、チェックされた項目がデータベースから削除されます。</p>
</footer>
<!--
<script id="todo-template" type="text/x-handlebars-template">
	{{#this}}
	<li {{#if completed}}class="completed"{{/if}} data-id="{{id}}">
		<div class="view">
			<input class="toggle" type="checkbox" {{#if completed}}checked{{/if}}>
			<label>{{title}}</label>
			<button class="destroy"></button>
		</div>
		<input class="edit" value="{{title}}">
	</li>
	{{/this}}
</script>
<script id="footer-template" type="text/x-handlebars-template">
	<span id="todo-count"><strong>{{activeTodoCount}}</strong> {{activeTodoWord}} left</span>
	<ul id="filters">
		<li>
			<a {{#eq filter 'all'}}class="selected"{{/eq}} href="/all">All</a>
		</li>
		<li>
			<a {{#eq filter 'active'}}class="selected"{{/eq}}href="/active">Active</a>
		</li>
		<li>
			<a {{#eq filter 'completed'}}class="selected"{{/eq}}href="/completed">Completed</a>
		</li>
	</ul>
	{{#if completedTodos}}<button id="clear-completed">Clear completed</button>{{/if}}
</script>
-->
