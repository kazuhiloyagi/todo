<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.net.URLEncoder, java.util.List, java.util.ArrayList, java.util.Random,
	  java.io.PrintWriter,
      db.ToDoPage, db.PageDAO" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Puchi ToDo</title>
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/handlebars.runtime-v4.0.10.js"></script>
<script type="text/javascript" src="https://rawgit.com/flatiron/director/master/build/director.min.js"></script>
<!-- <script type="text/javascript" src="js/app.js"></script> -->
<link rel="stylesheet" type="text/css" href="css/app.css">
</head>
<body>
<%-- ロゴ --%>
<a href=""><img src="img/logo.png" /></a>
<%-- ヘッダメニュー --%>
<!-- <a href="list">一覧</a>
<a href="create.jsp">新規</a> -->
<%-- メッセージ表示 --%>
<p>${message}</p>
<hr />
