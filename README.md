# README #

TODOアプリのWebアプリケーション版です。
https://github.com/tastejs/todomvc/tree/gh-pages/examples/jquery を参考にリライトさせてもらいました。

## What is this repository for? ##
本アプリケーションのMVC(Model-View-Controller)図 
![Model-View-Controller diagram](MVC.png)

* 新規のTOTOを入力してデータベースに保存します。
* 完了したTODOは左端をクリックして、チェックマークを付けます。
* 完了したTODOはまとめてデータベースから削除できます。
* ただし、今回のTODOアプリではセッション管理とユーザー認証を省いています。
* 今回のTODOアプリでは入力した内容の無害化(サニタイズ)を行っています。
* Current Version 0.1
* MIT License

### Dependencies ###
* お使いのOSの Tomcat 環境
    * Eclipse
    * jdbc ドライバ （postgresql-9.4.1212.jar）
    * com.sun.rowset.CachedRowSetImpl クラス
* PostgreSQL

### Database configuration ###
次に、WebアプリケーションTODOリスト登録用のテーブルを作成します。
```
CREATE TABLE todo_page (
    title VARCHAR(100) PRIMARY KEY, /* TODOリスト */
    content VARCHAR(200), /* TODOリストの詳細 */
    update_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP, /* 更新日時 */
    completed BOOLEAN DEFAULT FALSE, /* TODOの完了したかどうか */ 
    id CHAR(8) /* TODOリストのID */
);
```

### How to run ###
Webブラウザから「http://localhost:8080/todo/」にアクセスします

## Who do I talk to? ##

* kazuhiloyagi
* kazuhilo.yagi(atmark)gmail.com の(atmark)を@に変えて下さい