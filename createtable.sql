CREATE TABLE todo_page (
  title VARCHAR(100) PRIMARY KEY,
  content VARCHAR(200),
  update_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
INSERT INTO todo_page (title, content) VALUES (
  'welcome',
   'Hello!\n This is "PuchiToDo", a simple todo application. '
);
ALTER TABLE todo_page ADD COLUMN completed BOOLEAN DEFAULT false;
UPDATE todo_page SET completed = false;
ALTER TABLE todo_page ADD COLUMN id CHAR(8);
UPDATE todo_page SET id = '00000000' WHERE title = 'welcome';