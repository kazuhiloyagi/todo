package todo;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.PageDAO;
import db.ToDoPage;

/**
 * Servlet implementation class ListServlet
 */
public class ListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.removeAttribute("message");
		if(request.getAttribute("filter") == null) {
			request.setAttribute("filter", "all");
		}
		try {
			List<ToDoPage> list = PageDAO.getInstance().findAll();
			request.setAttribute("list", list);

			request.getRequestDispatcher("/list.jsp").forward(request, response);

		} catch(SQLException e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//		String menu = request.getParameter("menu");
//		request.setAttribute("filter", menu);
//
//		// POSTメソッドで入力パラメータがallだったら
//		if(menu == "all") {
//			try {
//				List<ToDoPage> list = PageDAO.getInstance().findAll();
//				request.setAttribute("list", list);
//
//				request.getRequestDispatcher("/list.jsp").forward(request, response);
//
//			} catch(SQLException e) {
//				throw new ServletException(e);
//			}
//
//		} else if(menu == "active") {
//			try {
//				List<ToDoPage> list = PageDAO.getInstance().findActiveTitles();
//				request.setAttribute("list", list);
//
//				request.getRequestDispatcher("/list.jsp").forward(request, response);
//
//			} catch(SQLException e) {
//				throw new ServletException(e);
//			}
//
//		} else if(menu == "completed") {
//			try {
//				List<ToDoPage> list = PageDAO.getInstance().findCompletedTitles();
//				request.setAttribute("list", list);
//
//				request.getRequestDispatcher("/list.jsp").forward(request, response);
//
//			} catch(SQLException e) {
//				throw new ServletException(e);
//			}
//
//		}

		doGet(request, response);
	}
}
