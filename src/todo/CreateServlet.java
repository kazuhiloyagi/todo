package todo;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.PageDAO;
import db.ToDoPage;

/**
 * Servlet implementation class CreateServlet
 */
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.removeAttribute("message");
		//String title = request.getParameter("title");
		//String content = request.getParameter("content");
		String title = (String)request.getParameter("new-title");
		String content = "";
		if(request.getParameter("content") != null) {
			content = (String)request.getParameter("content");
		}

		try {
			ToDoPage todoPage = new ToDoPage();
			todoPage.setTitle(title);
			if(content != null) {
				todoPage.setContent(content);
			}

			// idにランダムな8桁の数字を入れる
		 	Random rnd = new Random();
		    int ran = rnd.nextInt(100000000);
		 	String id = String.valueOf(ran);
		 	todoPage.setId(id);

		 	boolean completed = false;
		 	todoPage.setCompleted(completed);

			PageDAO.getInstance().insert(todoPage);
			request.setAttribute("message", title + "を作成しました。");

			request.getRequestDispatcher("/list").forward(request, response);

		} catch(SQLException e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
