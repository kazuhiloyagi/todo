package todo;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.PageDAO;
import db.ToDoPage;

/**
 * Servlet implementation class ActiveServlet
 */
public class ActiveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.removeAttribute("message");
		String menu = request.getParameter("menu");
		menu = "active";
		request.setAttribute("filter", menu);

		// GETメソッドで入力パラメータがacttiveだったら
		//if(menu == "active") {
			try {
				List<ToDoPage> list = PageDAO.getInstance().findActiveTitles();
				request.setAttribute("list", list);

				request.getRequestDispatcher("/list.jsp").forward(request, response);

			} catch(SQLException e) {
				throw new ServletException(e);
			}

		//}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
