package todo;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.PageDAO;
import db.ToDoPage;

/**
 * Servlet implementation class UpdateServlet
 */
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.removeAttribute("message");
		String title = (String)request.getParameter("title");
		String id = (String)request.getParameter("id");
		String content = "";
		if(request.getParameter("content") != null) {
			content = request.getParameter("content");
		}
		String checked = "0";
		if(request.getParameter("checked") != null) {
			checked = (String)request.getParameter("checked");
		}
		boolean completed;

		try {
			ToDoPage todoPage = new ToDoPage();
			todoPage.setId(id);
			todoPage.setTitle(title);
			if(content != null) {
				todoPage.setContent(content);
			}
			if(checked.equals("1")) {
				completed = true;
			} else {
				completed = false;
			}
			todoPage.setCompleted(completed);
			PageDAO.getInstance().update(todoPage);
			request.setAttribute("message", title + "を更新しました。");
			request.getRequestDispatcher("/list").forward(request, response);

		} catch (SQLException e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
