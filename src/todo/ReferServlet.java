package todo;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import db.PageDAO;
import db.ToDoPage;

/**
 * Servlet implementation class ReferServlet
 */
public class ReferServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("null")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.removeAttribute("message");
		String title = null;
		if(request.getParameter("title") != null) {
			title = request.getParameter("title");
		} else {
			title = "welcome";
		}

		try {
			if(title != null || title.length() > 0) {
				ToDoPage todoPage = PageDAO.getInstance().findByTitle(title);
				HttpSession session = request.getSession();
				session.setAttribute("todoPage", todoPage); // referページが呼ばれるたびにsession属性が更新されるようになっている
			}
			request.getRequestDispatcher("/refer.jsp").forward(request, response);

		} catch(SQLException e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
