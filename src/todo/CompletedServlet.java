package todo;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.PageDAO;
import db.ToDoPage;

/**
 * Servlet implementation class CompletedServlet
 */
public class CompletedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.removeAttribute("message");
		String menu = request.getParameter("menu");
		menu = "completed";
		request.setAttribute("filter", menu);

		//if(menu == "completed") {
			try {
				List<ToDoPage> list = PageDAO.getInstance().findCompletedTitles();
				request.setAttribute("list", list);

				request.getRequestDispatcher("/list.jsp").forward(request, response);

			} catch(SQLException e) {
				throw new ServletException(e);
			}

		//}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
