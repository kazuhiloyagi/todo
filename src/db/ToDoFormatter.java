package db;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ToDoFormatter {

	//データベース入力前にエスケープ処理をする
	public String formatText(String text) {
		text = escapeHtmlTags(text);
		text = escapeQuotation(text);

		return text;
	}

	//Webページ表示前にエスケープ処理をする
	public String formatHTML(String text) {
		text = escapeHtmlTags(text);
		text = createHyperLink(text);
		text = addBrToEachLines(text);
		text = escapeQuotation(text);

		return text;
	}

	// ", ' の変換
	private String escapeQuotation(String text) {
		text = text.replaceAll("/'/", "&#39;");
		text = text.replaceAll("/\'/", "&#39;");
		text = text.replaceAll("/\"/", "&quot;");

		return text;
	}

	//<br />タグの追加
	private String addBrToEachLines(String text) {
		text = text.replaceAll("/[\n]/", "<br />");
		text = text.replaceAll("/\n/", "<br />");
		text = text.replaceAll("/\\n/", "<br />");
		text = text.replaceAll("\\n", "<br />");
		text = text.replaceAll("[\n]", "<br />");

		return text;
	}

	//<,>,タブの変換
	private String escapeHtmlTags(String text) {
		text = text.replaceAll("/ /", "&nbsp;");// 追加：空白考慮
		text = text.replaceAll("/</", "&lt;");
		text = text.replaceAll("/>/", "&gt;");
		text = text.replaceAll("/\t/", "    ");
		text = text.replaceAll("\\t", "    ");
		text = text.replaceAll("/\\t/", "    ");

		return text;
	}

	//リンクの生成
	private String createHyperLink(String text) {
		Pattern pattern = Pattern.compile("(mailto|http|https|ftp):\\/\\/([^\\s]+)");
		Matcher matcher = pattern.matcher(text);
		StringBuffer sb = new StringBuffer();
		while(matcher.find()) {
			String group = matcher.group();
			String repText = "<a href=\"" + group + "\">" + group + "</a>";
			matcher.appendReplacement(sb, repText);
		}
		matcher.appendTail(sb);

		return sb.toString();
	}
}
