package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.sun.rowset.CachedRowSetImpl;

public class DBManager {

	public static Connection getConnection() { // クラスメソッド
		try {
			Class.forName("org.postgresql.Driver"); // これを記述しないとEclipseから PostgreSQLのドライバーが見つからない
			Connection con = DriverManager.getConnection(
					"jdbc:postgresql://localhost:5432/todo", // 接続先文字列
					"postgres", //DBユーザーID
					"postgres" // DBパスワード
					);

			return con;

		} catch(Exception e) {
			throw new IllegalStateException(e);
		}
	}

	public static void main(String[] args) throws SQLException {
		Connection con = getConnection();
		System.out.println("con=" + con);
		con.close();
	}

	/*
	 * 検索SQLを発行して、レコード数を返す。
	 */
	public static int simpleUpdate(String sql) throws SQLException {
		Connection con = null;
		Statement stm = null;

		try {
			con = getConnection();
			stm = con.createStatement();
			return stm.executeUpdate(sql); // 更新レコード数が返ってくる

		} finally {
			if (stm != null) {
				try {
					stm.close();
				} catch (SQLException ignore) {

				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ignore) {

				}
			}
		}
	}

	/*
	 * 検索SQLを発行して、結果をCachedRowSetImplに入れて返す。
	 */
	public static CachedRowSetImpl simpleFind(String sql) throws SQLException  {

		Connection con = null;
		Statement stm = null;

		try {
			con = getConnection();
			stm = con.createStatement();
			ResultSet rs = stm.executeQuery(sql);
			CachedRowSetImpl crs = new CachedRowSetImpl();
			crs.populate(rs);

			return crs;

		} finally {
			if (stm != null) {
				try {
					stm.close();
				} catch (SQLException ignore) {

				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ignore) {

				}
			}
		}
	}

}
