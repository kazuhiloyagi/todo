package db;

public class ToggleFilters {
	public String filter;

	public ToggleFilters(String filter) {
		super();
		setFilter(filter);
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}
}
