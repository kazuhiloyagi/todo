package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PageDAO {
	/**
	 * このクラスのインスタンスを取得
	 */
	public static PageDAO getInstance() {
		return new PageDAO();
	}

	/**
	 * 全文検索
	 * @return ToDoPageオブジェクトのList
	 */
	public List<ToDoPage> findAll() throws SQLException {
		String sql = "SELECT * FROM todo_page ORDER BY update_time DESC";
		ResultSet rs = (ResultSet)DBManager.simpleFind(sql);
		List<ToDoPage> list = new ArrayList<ToDoPage>();
		while(rs.next()) {
			ToDoPage page = new ToDoPage();
			page.setId(rs.getString("id"));
			page.setTitle(rs.getString("title"));
			page.setUpdateTime(rs.getTimestamp("update_time"));
			if(rs.getString("content") != null) {
				page.setContent(rs.getString("content"));
				page.setFormatedContent(page.getContent());
			}
			page.setCompleted(rs.getBoolean("completed"));
			list.add(page);
		}

		return list;
	}

	/**
	 * 完了したタイトルのレコードを検索
	 * @return 該当するレコード数
	 */
	public List<ToDoPage> findActiveTitles() throws SQLException {
		String sql = "SELECT * FROM todo_page WHERE completed = false";
		ResultSet rs = (ResultSet)DBManager.simpleFind(sql);
		List<ToDoPage> list = new ArrayList<ToDoPage>();
		while(rs.next()) {
			ToDoPage page = new ToDoPage();
			page.setId(rs.getString("id"));
			page.setTitle(rs.getString("title"));
			page.setUpdateTime(rs.getTimestamp("update_time"));
			if(rs.getString("content") != null) {
				page.setContent(rs.getString("content"));
				page.setFormatedContent(page.getContent());
			}
			page.setCompleted(rs.getBoolean("completed"));
			list.add(page);
		}

		return list;
	}

	/**
	 * 完了したタイトルのレコードを検索
	 * @return 該当するレコード数
	 */
	public List<ToDoPage> findCompletedTitles() throws SQLException {
		String sql = "SELECT * FROM todo_page WHERE completed = true";
		ResultSet rs = (ResultSet)DBManager.simpleFind(sql);
		List<ToDoPage> list = new ArrayList<ToDoPage>();
		while(rs.next()) {
			ToDoPage page = new ToDoPage();
			page.setId(rs.getString("id"));
			page.setTitle(rs.getString("title"));
			page.setUpdateTime(rs.getTimestamp("update_time"));
			if(rs.getString("content") != null) {
				page.setContent(rs.getString("content"));
				page.setFormatedContent(page.getContent());
			}
			page.setCompleted(rs.getBoolean("completed"));
			list.add(page);
		}

		return list;
	}

	/**
	 * 指定したタイトルに一致するレコードを検索
	 * @param title
	 * @return ToDoPageオブジェクト
	 */
	public ToDoPage findByTitle(String title) throws SQLException {
		 String sql = "SELECT * FROM todo_page WHERE title = '" + title + "'";
		 ResultSet rs = (ResultSet)DBManager.simpleFind(sql);
		 List<ToDoPage> list = new ArrayList<ToDoPage>();
		while(rs.next()) {
			ToDoPage page = new ToDoPage();
			page.setId(rs.getString("id"));
			page.setTitle(rs.getString("title"));
			page.setUpdateTime(rs.getTimestamp("update_time"));
			if(rs.getString("content") != null) {
				page.setContent(rs.getString("content"));
				page.setFormatedContent(page.getContent());
			}
			page.setCompleted(rs.getBoolean("completed"));
			list.add(page);
		}

		 if(list.size() == 0) {
			 return null;

		 } else if(list.size() > 1) {
			 throw new IllegalArgumentException("record > 0");
		 }
		 return list.get(0);
	 }

	 /**
	  * 指定したToDoPageを元にINSERTを実行します
	  * @param ToDoPageオブジェクト
	  */
	 public void insert(ToDoPage page) throws SQLException {
		 //データベース入力前にエスケープ処理をする
		 page.setId(page.getId());
		 page.setTitle(page.getTitle());
		 page.setCompleted(page.isCompleted());
		 String sql;
		 if(page.getContent() != null) {
			 page.setContent(page.getContent());
			 page.setFormatedContent(page.getContent());

			 sql = "INSERT INTO todo_page (title, content, completed, id) VALUES('"
					 + page.getTitle() + "', '" + page.getContent() + "', '"
					 + page.isCompleted() + "','" + page.getId() + "')";
		 } else {
			 sql = "INSERT INTO todo_page (title, completed, id) VALUES('"
					 + page.getTitle()  + "', '"
					 + page.isCompleted() + "','" + page.getId() + "')";
		 }
		 DBManager.simpleUpdate(sql);
	 }

	 /**
	  * 指定したToDoPageを元にUPDATEを実行します
	  * @param ToDoPageオブジェクト
	  */
	 public void update(ToDoPage page) throws SQLException {
		 //データベース入力前にエスケープ処理をする
		 page.setId(page.getId());
		 page.setTitle(page.getTitle());
		 page.setCompleted(page.isCompleted());
		 String sql;
		 if(page.getContent() != null) {
			 page.setContent(page.getContent());
			 page.setFormatedContent(page.getContent());

			 sql = "UPDATE todo_page SET content='" + page.getContent()
			 		+ "' , completed='" + page.isCompleted() + "' , id='" + page.getId()
		 		 	+ "' WHERE title='" + page.getTitle() + "'";
		 } else {
			 sql = "UPDATE todo_page SET completed='" + page.isCompleted() + "' , id='" + page.getId()
	 		 	+ "' WHERE title='" + page.getTitle() + "'";
		 }
		 DBManager.simpleUpdate(sql);
	 }

	 /**
	  * 指定したToDoPageを元にDELETEを実行します
	  * @param ToDoPageオブジェクト
	  */
	 public void delete(ToDoPage page) throws SQLException {
		 String sql = "DELETE FROM todo_page WHERE title='" + page.getTitle() + "'";
		 DBManager.simpleUpdate(sql);
	 }
}
