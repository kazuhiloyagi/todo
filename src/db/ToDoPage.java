package db;

import java.sql.Timestamp;

public class ToDoPage {

	/* データベースの列ID */
	private String id;
	/* ページの名前 */
	private String title;
	/* ページの内容v*/
	private String content;
	/* 更新日時 */
	private Timestamp updateTime;
	/* ページの整形済み内容 */
	private String formatedContent;
	/* TODOが完了したかどうか
	 *  true : 完了した */
	private boolean completed;



	public String getId() {
		return id;
	}

	public void setId(String id) {
		//データベース入力前にエスケープ処理をする
		ToDoFormatter formatter = new ToDoFormatter();
		this.id = formatter.formatText(id);
		System.out.println("id:" + this.id);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		//データベース入力前にエスケープ処理をする
		ToDoFormatter formatter = new ToDoFormatter();
		this.title = formatter.formatText(title);
		System.out.println("title:" + this.title);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		//データベース入力前にエスケープ処理をする
		ToDoFormatter formatter = new ToDoFormatter();
		this.content = formatter.formatText(content);
		System.out.println("content:" + this.content);
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public String getFormatedContent() {
		return formatedContent;
	}

	public void setFormatedContent(String content) {
		//Webページ表示前にエスケープ処理をする
		ToDoFormatter formatter = new ToDoFormatter();
		this.formatedContent = formatter.formatHTML(content);
		System.out.println("formatContent:" + this.formatedContent);
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	// 未検査の型変換警告の対策
	@SuppressWarnings("unchecked")
	public static <T> T autocast(Object obj) {
		T castobj = (T)obj;
		return castobj;
	}

}
